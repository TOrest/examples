package operations;

public class Operations {

    public void compareFourNumbers(int a, int b, int c, int d)
    {
        if(a > b && a > c && a > d)
        {
            System.out.println( a + " the biggest number");
        }
        else if(b > a && b > c && b > d)
        {
            System.out.println(b + " the biggest number");
        }
        else if(c > a && c > b && c > d)
        {
            System.out.println(c + " the biggest number");
        }
        else
        {
            System.out.println(d + " the biggest number");
        }
    }


    public int[][] deletingMinRow(int[][] array)
    {
        int sum = 0;
        int min = 1000000;
        int index = 0;
        int [] sum_array = new int[array.length];
        int[][] array_result = new int[array.length - 1][array[0].length];

        for(int i = 0; i < array.length; i++)
        {
            for(int j = 0; j < array[i].length; j++)
            {
                sum += array[i][j];
            }
            sum_array[i] = sum;
            sum = 0;
        }

        for(int i = 0; i < sum_array.length; i++)
        {
            if(min > sum_array[i])
            {
                min = sum_array[i];
                index = i;
            }
        }

        for(int i = 0; i < array.length; i++)
        {
            if(i < index)
            {
                for (int j = 0; j < array[i].length; j++)
                {
                    array_result[i][j] = array[i][j];
                }
            }
            else
            {
                if(index == array.length - 1 || i == array.length - 1)
                {
                    break;
                }
                else
                {
                    for(int j = 0; j < array[i].length; j++)
                    {
                        array_result[i][j] = array[i+1][j];
                    }
                }
            }
        }

        return array_result;
    }

    public void print2DArray(int [][] array)
    {
        for(int i = 0; i < array.length; i++)
        {
            for(int j = 0; j < array[i].length; j++)
            {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }


}

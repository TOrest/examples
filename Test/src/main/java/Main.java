import operations.*;

public class Main {

    public static void main(String[] args) {
        Operations action = new Operations();
        action.compareFourNumbers(152,367,490,120);
        int [][] array = {{67, 23, 34, 21, 55},{14, 89, 67, 90, 23},{12, 32, 12, 45, 32},{67, 21, 85, 75, 100}};
        int [][] array_changed = action.deletingMinRow(array);
        action.print2DArray(array_changed);
    }
}
